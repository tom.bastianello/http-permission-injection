// Request module, used to perform remote api requests.
const request = require('request');

// Cookie module used to set/retreive cookies.
const cookie = require('cookie');

// Cryptr module used to encrypt cookies.
const Cryptr = require('cryptr');

var cryptr = null;

// Module Definition.
module.exports = {
    is_source_remote: false,
    remote_token: "changeme",
    remote_endpoint: "http://localhost/get_user_permissions",
    user_data: null,
    cookie_encryption_key: "changeme",
    cookie_expiry_minutes: 60,
    cookie_prefix: "auth_",
    user_secure_username_cookie: false,
    cookie_path: "/",
    use_global_user_data: true,
    use_global_cookie_prefix: true,

    /*
        Perform an api call to the given remote_endpoint using the remote_token, fetching an object containing user permissions.
        "callback" must refer to a function to be executed once the async task is completed.
    */
    fetchRemoteUsers: function(callback) {
        if(this.is_source_remote){
            remoteAPIRequest(
                {
                    callback: callback,
                    remote_endpoint: this.remote_endpoint,
                    remote_token: this.remote_token
                }
            );
        }else{
            callback(null);
        }
    },

    /*
        Use a given username to inject account permissions to a secure cookie;
        Requirements - "event" must contain: 'request', 'response', 'success_url', 'denied_url'.
    */
    injectPermissions: function(event){
        try{
            var cookie_prefix = this.cookie_prefix;
            if(!this.use_global_cookie_prefix){
                cookie_prefix = event.cookie_prefix;
            }
            if(event.username){
                // Username is there 
            }else{
                denied(event);
            }
            
            if (cryptr == null){
                cryptr = new Cryptr(this.cookie_encryption_key);
            }
            // If 'user_secure_username_cookie' is set to true, get the username from a secure cookie.
            if(this.user_secure_username_cookie){
                try {
                    var cookies = cookie.parse(event.request.headers.cookie);
                    event.username = cryptr.decrypt(cookies[cookie_prefix + "username"]);
                }catch (err){
                    denied(event);
                }
            }
            var user_data = [];
            if (this.use_global_user_data) {
                user_data = this.user_data;
            }else{
                user_data = event.user_data;
            }
            var verified_user_object = null;
            if (typeof user_data == "string") {
                user_data = JSON.parse(user_data);
            }
            for (var i = 0; i < user_data.length; i++) {
                if(user_data[i].username == event.username.toLowerCase()){
                    verified_user_object = user_data[i];
                    break;
                }
            }
            if(verified_user_object != null){
                if (typeof user_data == "object") {
                    user_data = JSON.stringify(user_data);
                }
                event.permission_cookie = cryptr.encrypt(JSON.stringify(verified_user_object));
                event.cookie_expiry_minutes = this.cookie_expiry_minutes;
                event.cookie_prefix = cookie_prefix;
                event.cookie_path = this.cookie_path;
                approved(event);
            }else {
                denied(event);
            }
        }catch (err){
            denied(event);
        }        
    },

    /*
        Verify that the provided encrypted permission cookie has the required permissions to perform a task;
        Requirements - "event" must contain: 'request', 'response', 'callback'.
    */
    getPermissions: function(event){
        var cookie_prefix = this.cookie_prefix;
        if(!this.use_global_cookie_prefix){
            cookie_prefix = event.cookie_prefix;
        }

        if (cryptr == null){
            cryptr = new Cryptr(this.cookie_encryption_key);
        }
        var cookies = cookie.parse(event.request.headers.cookie);
        var permissions = [];
        try{
            permissions = cryptr.decrypt(cookies[cookie_prefix + "permissions"]);
        }catch (err){
            //console.log("[WARNING]: Permissions could not be retreived. Either the cookie is not set, or it is in the incorrect format.");
        }
        if (typeof permissions == "string") {
            permissions = JSON.parse(permissions);
        }
        event.callback(permissions);
    }
}

function approved (event){
    var now = new Date();
    now.setTime(now.getTime() + (event.cookie_expiry_minutes * 60 * 1000));
    event.response.writeHead(302, {
        'Set-Cookie': [event.cookie_prefix + "permissions=" + event.permission_cookie + ";expires="+now.toUTCString()+";path=" + event.cookie_path],
        'Content-Type': 'text/plain',
        "Location": event.success_url
    });
    event.response.end();
}

function denied (event){
    event.response.writeHead(302, {
        'Content-Type': 'text/plain',
        "Location": event.denied_url
    });
    event.response.end();
}

// Perform the remote API request to get the user list and return it to the callback funcion.
function remoteAPIRequest (event) {
    request.get({url: event.remote_endpoint, headers: {"Authorization":"Bearer " + event.remote_token, "Content-type":"application/json"}}, function(err, response, body){
        /*
            This function attempts to parse the data to JSON directly so that it is returned as an object.
            If your API does not return a JSON object, you may parse the data later in your callback function.
            If the data is parsed correctly, it is already set to the user_data object.
        */

        try{
            event.callback(JSON.parse(body));
        }catch (err){
            event.callback(body);
        }
    });
}